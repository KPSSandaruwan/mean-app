import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from 'rxjs';
import { Post } from "./post.model";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";


@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

  constructor(private http: HttpClient, private router: Router) {

  }

  getPosts(postsPerPage: number, currentPage: number) {
    // return [...this.posts]; // Spread operator
    // return this.posts;
    const queryParams = `?pageSize=${postsPerPage}&page=${currentPage}`
    this.http
    .get<{ message: string, posts: any [], maxPosts: number} >('http://localhost:3000/api/posts' + queryParams)
    .pipe(
      map(postData => {
        return { posts: postData.posts.map(post => {
          return {
            title: post.title,
            content: post.content,
            id: post._id,
            creator: post.creator
          };
      }), maxPosts: postData.maxPosts};  // wrapped into the observable
    }))

    .subscribe((transformedPostData) => {
        this.posts = transformedPostData.posts;
        this.postsUpdated.next({
          posts: this.posts,
          postCount: transformedPostData.maxPosts
        });
        // this.postsUpdated.next([...this.posts]);
      });
  }

  getPostUpdateListnr() {
    return this.postsUpdated.asObservable();
  }

  getPost(id: string) {
    return this.http
      .get<{_id: string, title: string, content: string, creator: string} >("http://localhost:3000/api/posts/" + id);
  }

  addPost(title: string, content: string) {
    const post: Post = { id: null as any, title: title, content: content, creator: null as any };
    this.http
      .post<{ message: string, postId: string }>("http://localhost:3000/api/posts", post)
      .subscribe((responseData) => {
        // console.log(responseData.message);
        // const id = responseData.postId;
        // post.id = id;
        // this.posts.push(post);
        // this.postsUpdated.next(this.posts);
        this.router.navigate(["/"])
      });
      // this.posts.push(post);
      // this.postsUpdated.next(this.posts);
      // this.postsUpdated.next([...this.posts]);
  }

  updatePost(id: string, title:string, content: string) {
    let postData: Post | FormData;

    postData = {
      id: id,
      title: title,
      content: content,
      creator: null
    };
    this.http.put("http://localhost:3000/api/posts/" + id, postData)
    .subscribe(response => {
      ///////////////////////////////////////////////////////////
      // This codes doesn't because navigatation will take care of fetching
      // the new posts
      ///////////////////////////////////////////////////////

      // const updatedPosts = this.posts;
      // const oldPostIndex = updatedPosts.findIndex(post => post.id === post.id);
      // updatedPosts[oldPostIndex] = post;
      // this.posts = updatedPosts;
      // this.postsUpdated.next(this.posts);

      this.router.navigate(["/"])
    });
  }

  deletePost(postId: string) {
    // this.http.delete("http://localhost:3000/api/posts/" + postId)
    // .subscribe(() => {
    //   // console.log('Deleted!');
    //   const updatedPosts = this.posts.filter(post => post.id !== postId);
    //   this.posts = updatedPosts;
    //   this.postsUpdated.next(this.posts);
    // });

    return this.http
      .delete("http://localhost:3000/api/posts/" + postId)
  }
}

